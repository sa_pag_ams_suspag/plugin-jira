## Sustentação JS

O intuito do repositório é ser um mecanismos de apoio ao time Xangai e Dalian nos atendimentos de chamados através do Jira


## Instalação do Script
1. Instalar o Violent Monkey's como plugin do navegador 
2. Abrir e copiar o link do plugin desejado (https://bitbucket.org/sa_pag_ams_suspag/plugin-jira/raw/master/Alert%20-%20N1 ou https://bitbucket.org/sa_pag_ams_suspag/plugin-jira/raw/master/Alert%20-%20N2)
3. Na extensão do navegador abrir o Violent Monkey's
4. Clicar em '+' e depois em 'Install from URL'
5. Confirme a instalação e pronto.

Documentação do processo: https://jiraps.atlassian.net/wiki/spaces/PX/pages/70772459296/Plugin+Jira+-+Violent+Monkey


**Importante: Não alterar entre // ==UserScript== **